# Wordle Solver

A program for assisting players in playing the game [Wordle](https://www.nytimes.com/games/wordle/index.html).

Still under active development, so the code is a bit messy and unoptimized.

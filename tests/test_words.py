import pytest

import wordle_assistant.words as words


@pytest.mark.parametrize('possible,impossible,guessable', [(words.POSSIBLE, words.IMPOSSIBLE, words.GUESSABLE),
                                                           (words.POSSIBLE_WORDS, words.IMPOSSIBLE_WORDS,
                                                            words.GUESSABLE_WORDS)])
def test_word_sets(possible, impossible, guessable):
    assert len(possible & impossible) == 0
    assert (possible | impossible) == guessable

    for word in possible:
        assert word in guessable
        assert len(word) == words.WORD_LENGTH

    for word in impossible:
        assert word in guessable
        assert len(word) == words.WORD_LENGTH

    for word in guessable:
        assert (word in possible) or (word in guessable)
        if word in possible:
            assert word not in impossible
        else:
            assert word not in possible

        assert len(word) == words.WORD_LENGTH

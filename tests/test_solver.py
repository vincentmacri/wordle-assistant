import statistics

import pytest

import wordle_assistant.game_board as game_board
import wordle_assistant.words as words


def teardown():
    counts = []
    for word in sorted(words.POSSIBLE_WORDS):
        board = game_board.GameBoard(correct_word=word)
        board.solve_self()
        counts.append(board.guess_count)

    # game_board.GameBoard.clear_best_guess_cache()
    # start_word = game_board.GameBoard().best_next_guess()
    # game_board.GameBoard.clear_best_guess_cache()
    # start_word_counting = game_board.GameBoard(remaining_words=words.POSSIBLE_WORDS_COUNTED).best_next_guess()

    with open('metrics.txt', 'w') as f:
        f.write(f'mean_guesses {statistics.mean(counts)}\n')
        f.write(f'median_guesses {statistics.median(counts)}\n')
        f.write(f'1_guess {counts.count(1)}\n')
        f.write(f'2_guess {counts.count(2)}\n')
        f.write(f'3_guess {counts.count(3)}\n')
        f.write(f'4_guess {counts.count(4)}\n')
        f.write(f'5_guess {counts.count(5)}\n')
        f.write(f'6_guess {counts.count(6)}\n')
        # f.write(f'start_word {start_word}\n')
        # f.write(f'start_word_counting {start_word_counting}\n')


@pytest.fixture(autouse=True, scope='session')
def setup_teardown():
    yield
    teardown()


@pytest.mark.parametrize('word', sorted(words.POSSIBLE_WORDS), ids=str)
def test_word(word):
    board = game_board.GameBoard(correct_word=word)
    board.solve_self()
    assert board.guess_count <= 6, word
    assert 1 <= board.guess_count


if __name__ == '__main__':
    import cProfile, pstats, io

    pr = cProfile.Profile()
    tested = 0
    pr.enable()
    for w in sorted(words.POSSIBLE_WORDS):
        print(w, len(game_board.GameBoard.best_guess_cache), len(game_board.GameBoard.add_guess_cache))
        test_word(w)
        tested += 1
        if tested >= 3:
            break
    pr.disable()

    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats(pstats.SortKey.CUMULATIVE)
    ps.print_stats()
    print(s.getvalue())

from __future__ import annotations

from datetime import date

from .word import Word
from .word_lists import POSSIBLE_LIST, IMPOSSIBLE_LIST

WORD_LENGTH = 5

initial_size = len(POSSIBLE_LIST) + len(IMPOSSIBLE_LIST)
wordle_day = date.today().toordinal() - 737960

IMPOSSIBLE: set[str] = set(sorted(IMPOSSIBLE_LIST + POSSIBLE_LIST[:wordle_day]))
POSSIBLE: set[str] = set(sorted(POSSIBLE_LIST[wordle_day:]))
GUESSABLE: set[str] = POSSIBLE | IMPOSSIBLE

POSSIBLE_WORDS: set[Word] = {Word(word) for word in POSSIBLE_LIST}
IMPOSSIBLE_WORDS: set[Word] = {Word(word) for word in IMPOSSIBLE_LIST}
GUESSABLE_WORDS: set[Word] = POSSIBLE_WORDS | IMPOSSIBLE_WORDS

POSSIBLE_WORDS_COUNTED: set[Word] = {Word(word) for word in POSSIBLE}
IMPOSSIBLE_WORDS_COUNTED: set[Word] = {Word(word) for word in IMPOSSIBLE}


def get_word(word: str) -> Word:
    for w in GUESSABLE_WORDS:
        if w.word == word:
            return w
    assert False, f'{word} was not in GUESSABLE_WORDS'


BEST_WORD = get_word('soare')
BEST_WORD_COUNTED = get_word('raise')

assert len(GUESSABLE) == initial_size
assert len(GUESSABLE_WORDS) == initial_size
assert len(POSSIBLE) == len(POSSIBLE_WORDS_COUNTED)
assert len(IMPOSSIBLE) == len(IMPOSSIBLE_WORDS_COUNTED)
assert len(GUESSABLE) == len(GUESSABLE_WORDS)

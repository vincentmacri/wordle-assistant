from typing import Optional

from .guess import Guess
from .letter_status import Status, LetterStatus
from .word import Word
from .words import GUESSABLE_WORDS, WORD_LENGTH, POSSIBLE_WORDS, BEST_WORD


class GameBoard:
    best_guess_cache: dict[tuple[Guess, ...], Word] = dict()
    add_guess_cache: dict[tuple[Guess, tuple[Guess, ...]], set[Word]] = dict()

    def __init__(self, remaining_words: Optional[set[Word]] = None, correct_word: Optional[Word] = None):
        self.remaining_words: set[Word]
        if remaining_words is None:
            self.remaining_words = POSSIBLE_WORDS
        else:
            self.remaining_words = remaining_words

        self.correct_word = correct_word
        self._guesses: list[Guess] = []

    def solve_self(self, first_word: Word = BEST_WORD) -> None:
        assert self.correct_word is not None

        self.add_guess(self.auto_make_guess(first_word, self.correct_word))

        while not self.is_solved():
            guess_word = self.best_next_guess()
            self.add_guess(self.auto_make_guess(guess_word, self.correct_word))

        assert len(self.remaining_words) == 1
        assert self._guesses[-1].word == self.correct_word.word

    def _best_next_guess(self) -> Optional[Word]:
        remaining_guesses_count = 6 - self.guess_count
        remaining_words_count = len(self.remaining_words)

        if remaining_words_count <= 2:  # remaining_guesses_count:
            if len(self.remaining_words) == 0:
                return None
            best_word = max(self.remaining_words, key=self.word_score)
        else:
            best_word = max(sorted(GUESSABLE_WORDS), key=self.word_score)

        if self.guess_count <= 4:
            self.best_guess_cache[tuple(self._guesses)] = best_word

        return best_word

    def best_next_guess(self) -> Word:
        if tuple(self._guesses) in self.best_guess_cache:
            return self.best_guess_cache[tuple(self._guesses)]

        best_word = self._best_next_guess()
        assert best_word is not None, f'{self.correct_word}, {self._guesses}'

        return best_word

    @property
    def guess_count(self) -> int:
        return len(self._guesses)

    def is_solved(self) -> bool:
        assert self.correct_word is not None
        for guess in self._guesses:
            if guess.word == self.correct_word.word:
                assert self._guesses[-1].word == self.correct_word.word
                return True
        return False

    def add_guess(self, guess: Guess) -> None:
        cache_lookup = (guess, tuple(self._guesses))
        if cache_lookup in self.add_guess_cache:
            self.remaining_words = self.add_guess_cache[cache_lookup]
        else:
            self.remaining_words = self.remaining_words & Guess.matches(guess, self.remaining_words)
            if self.guess_count <= 2:
                self.add_guess_cache[cache_lookup] = self.remaining_words
        self._guesses.append(guess)

    @classmethod
    def auto_make_guess(cls, guess_word: Word, correct_word: Word) -> Guess:
        letter_statuses = []
        for i, letter in enumerate(guess_word):

            letter_status = Status.NONE
            if correct_word[i] == letter:
                letter_status = Status.CORRECT
            elif letter in correct_word:
                position_letter_count = 0
                correct_letter_count = 0  # How many times this letter appears correctly in the guess
                for k in range(WORD_LENGTH):
                    if guess_word[k] == letter:
                        if correct_word[k] == letter:
                            correct_letter_count += 1
                        elif k <= i:
                            position_letter_count += 1

                if position_letter_count + correct_letter_count <= correct_word.count(letter):
                    letter_status = Status.POSITION

            letter_statuses.append(LetterStatus(letter, letter_status))

        return Guess(letter_statuses)

    @classmethod
    def manual_make_guess(cls, word: Word, result: tuple[Status]) -> Guess:
        assert len(result) == len(word) == WORD_LENGTH

        letter_statuses = []
        for c, s in zip(word, result):
            letter_statuses.append(LetterStatus(c, s))

        return Guess(letter_statuses)

    def word_score(self, word: Word) -> float:
        # print(word)
        assert isinstance(word, Word), (word.__repr__(), type(word))

        word_score = 0
        for possible in self.remaining_words:
            guess = GameBoard.auto_make_guess(word, possible)
            new_remaining = self.remaining_words & Guess.matches(guess, self.remaining_words)
            word_score += len(self.remaining_words) - len(new_remaining)

        if word in self.remaining_words:
            word_score += 0.5

        return word_score

    @classmethod
    def clear_best_guess_cache(cls):
        cls.best_guess_cache = dict()
        cls.add_guess_cache = dict()

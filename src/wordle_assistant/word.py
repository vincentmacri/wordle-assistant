from __future__ import annotations


class Word:
    _occurrences_map: dict[tuple[str, int], set[Word]] = dict()

    def __init__(self, word: str):
        self.word: str = word
        self._counts: dict[str, int] = dict()

        for c in range(ord('a'), ord('z') + 1):
            char = chr(c)
            count = self.word.count(char)
            self._counts[char] = count
            if (char, count) not in self._occurrences_map:
                self._occurrences_map[(char, count)] = set()
            self._occurrences_map[(char, count)].add(self)

    @classmethod
    def word_letter_occurrences(cls, letter: str, occurrences: int) -> set[Word]:
        return cls._occurrences_map.setdefault((letter, occurrences), set())

    def count(self, char):
        return self._counts[char]

    def __len__(self):
        return len(self.word)

    def __hash__(self):
        return hash(self.word)

    def __str__(self):
        return self.word

    def __repr__(self):
        return f'Word({self.word})'

    def __eq__(self, other):
        return self.word == other.word

    def __getitem__(self, item):
        return self.word[item]

    def __lt__(self, other):
        return self.word < other.word

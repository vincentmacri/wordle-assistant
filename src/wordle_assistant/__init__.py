from . import words
from .game_board import GameBoard
from .guess import Guess
from .letter_status import LetterStatus, Status
from .words import POSSIBLE_WORDS_COUNTED


def play():
    board = GameBoard(remaining_words=POSSIBLE_WORDS_COUNTED)
    print(f'{len(board.remaining_words)} remaining words')
    print(f'Recommended word: {words.BEST_WORD_COUNTED}')

    while True:
        valid_word = False
        while not valid_word:
            word = input('Enter your guess: ')
            valid_word = word in words.GUESSABLE
            if not valid_word:
                print(f'{word} is not a valid word.')

        invalid_result = True
        while invalid_result:
            result = input('Enter the feedback (e.g. format: gynng): ')
            invalid_result = len(result) != words.WORD_LENGTH
            if invalid_result:
                print(f'{result} is not {words.WORD_LENGTH} long.')
                continue

            for c in result:
                if c not in 'gynb':
                    invalid_result = True
                    print(f'{c} is not a valid result character.')
                    break

        assert len(word) == len(result)

        if result == 'g' * words.WORD_LENGTH:
            break

        letter_statuses = []
        for c, s in zip(word, result):

            if s == 'g':
                status = Status.CORRECT
            elif s == 'y':
                status = Status.POSITION
            else:
                status = Status.NONE

            letter_statuses.append(LetterStatus(c, status))

        board.add_guess(Guess(letter_statuses))

        board_remaining_words = sorted(board.remaining_words)
        print(f'{len(board.remaining_words)} remaining words: {board_remaining_words}')
        print(f'Recommended word: {board.best_next_guess()}')


def find_best_starting_word(counted: bool = True):
    import random
    random.seed(1)

    if bool:
        board = GameBoard(remaining_words=POSSIBLE_WORDS_COUNTED)
    else:
        board = GameBoard()
    board = GameBoard(remaining_words=set(random.choices(list(POSSIBLE_WORDS_COUNTED), k=25)))

    start_word = board.best_next_guess()
    print(f'The best starting word is: {start_word}')

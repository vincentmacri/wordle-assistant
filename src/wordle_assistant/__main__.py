import cProfile
import io
import pstats

from . import play, find_best_starting_word

if __name__ == '__main__':
    pr = cProfile.Profile()
    pr.enable()

    play()
    #find_best_starting_word()

    pr.disable()

    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats(pstats.SortKey.CUMULATIVE)
    ps.print_stats()
    print(s.getvalue())

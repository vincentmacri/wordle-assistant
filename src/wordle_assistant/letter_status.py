from dataclasses import dataclass
from enum import Enum, auto

from . import words


class Status(Enum):
    NONE = auto()
    POSITION = auto()
    CORRECT = auto()


@dataclass(frozen=True)
class LetterStatus:
    letter: str
    status: Status


status_combinations = [[Status.NONE], [Status.POSITION], [Status.CORRECT]]
for i in range(1, words.WORD_LENGTH):
    new_status_combinations = []
    for status in status_combinations:
        for s in Status:
            new_status_combinations.append(status + [s])
    status_combinations = new_status_combinations

for i, status_combination in enumerate(status_combinations):
    status_combinations[i] = tuple(status_combination)

STATUS_COMBINATIONS: set[tuple[Status]] = set(status_combinations)
assert len(STATUS_COMBINATIONS) == len(Status) ** words.WORD_LENGTH

from __future__ import annotations

from typing import Union, Optional

from .letter_status import LetterStatus, Status
from .word import Word
from .words import WORD_LENGTH, POSSIBLE_WORDS


class Guess:

    def __init__(self, statuses: Union[list[LetterStatus], tuple[LetterStatus]]):
        self.statuses: tuple[LetterStatus]
        if isinstance(statuses, tuple):
            self.statuses = statuses
        else:
            self.statuses = tuple(statuses)

    def occurrences(self, letter: str) -> int:
        count = 0
        for ls in self:
            if ls.letter == letter and ls.status is not Status.NONE:
                count += 1
        return count

    @classmethod
    def matches(cls, guess: Guess, remaining_words: Optional[set[Word]] = None) -> set[Word]:
        if remaining_words is None:
            remaining_words = POSSIBLE_WORDS

        for i, letter_status in enumerate(guess):

            letter = letter_status.letter
            status = letter_status.status

            letter_count = 0  # Number of times letter correctly appears in guess
            for j in range(i):
                if guess[j].letter == letter and guess[j].status is not Status.NONE:
                    letter_count += 1

            if status is Status.NONE:
                total_occurrences = guess.occurrences(letter)
                remaining_words = remaining_words & Word.word_letter_occurrences(letter, total_occurrences)
            else:
                letter_count += 1

                for j in range(0, letter_count):
                    remaining_words = remaining_words - Word.word_letter_occurrences(letter, j)

                if status is Status.CORRECT:
                    remaining_words = {word for word in remaining_words if word[i] == letter}
                else:
                    # status is Status.POSITION
                    remaining_words = {word for word in remaining_words if word[i] != letter}

        return remaining_words

    @property
    def word(self) -> str:
        word = ''
        for ls in self:
            word += ls.letter
        assert len(word) == WORD_LENGTH
        return word

    def __eq__(self, other):
        return self.statuses == other.statuses

    def __hash__(self):
        return hash(self.statuses)

    def __getitem__(self, item):
        return self.statuses[item]

    def __repr__(self) -> str:
        statuses = ''
        for s in self:
            statuses += s.status.name[0]
        return f'({self.word}, {statuses})'
